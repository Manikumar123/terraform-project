variable "base_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    # <name>     = number
  })
}

variable "namespace_quota_max_cpu_requests" {
  type    = string
  default = "2"
}

variable "namespace_quota_max_memory_limits" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}
